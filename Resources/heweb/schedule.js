/**
 * This file is part of HEWEB11 Mobile.
 *
 * HEWEB11 Mobile is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HEWEB11 Mobile is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HEWEB11 Mobile.  If not, see <http://www.gnu.org/licenses/>.
 *
 * The HEWEB11 mobile app was based off the original work done by the team
 * at palatir.net which included:
 *
 * CODESTRONG code can be located at: https://github.com/appcelerator/Codestrong
 * DRUPALCON code can be located at: https://github.com/palantirnet/drupalcon_mobile
 *
 */

var hew = {};
(function() {
	hew.schedule = {
		/*
		 * getSessions
		 * params: _args (OBJECT)
		 * sub params: recent (STRING), track (STRING), date (STRING), success (CALLBACK), onerror (CALLBACK)
		 * return: on success or error callback funtion with sessions (OBJECT) as the param
		 */
		getSessions : function(/*OBJECT*/_args) {

			//make sure we're online before starting up the xhr
			if(Ti.Network.online) {
				var params = '';
				if(_args.track) {
					params = _args.track;
					if(_args.date) {
						params = params + "&" + _args.date;
					} else if(_args.recent) {
						params = params + "&" + _args.recent;
					}
				} else if(_args.recent) {
					params = _args.recent;
				} else if(_args.date) {
					params = _args.date;
				}

				var xhr = Titanium.Network.createHTTPClient();

				var url = "https://api.uwm.edu/index.cfm/hew/schedule?" + params + "&format=json";
				var response;
				xhr.open('GET', url);

				xhr.onerror = function(e) {
					//log error
					Ti.API.error(e);
					var isError =[];
					var noData = {};
					noData.title = "could not retrieve session data, please try again later";
					noData.description = "";
					noData.track = "";
					noData.sessioncode = "";
					noData.sessionnumber = "";
					noData.track = "";
					noData.end = "2011-10-23 00:00:00";
					noData.start = "2011-10-23 00:00:00";

					isError.push(noData);

					// if callback was provided run it
					if(_args.success) {
						_args.success(isError);
					}

				};

				xhr.onload = function() {
					try {
						response = JSON.parse(this.responseText);
						var sessions = [];
						if(response["DATA"].length >= 1) {
							// clean up the output and return it
							for(var i = 0; i < response["DATA"].length; i++) {
								// grab date so they can be cleaned
								var end_ts = response["DATA"][i][5];
								var start_ts = response["DATA"][i][6];

								var session = {};
								session.title = response["DATA"][i][0];
								session.description = response["DATA"][i][1];
								session.track = response["DATA"][i][2];
								session.sessioncode = response["DATA"][i][3];
								session.end = end_ts.substring(5, 24);
								session.start = start_ts.substring(5, 24);
								session.uid = response["DATA"][i][7];
								session.sessionnumber = response["DATA"][i][4];
								session.track = response["DATA"][i][6];
								sessions.push(session);
							}

							// if callback was provided run it
							if(_args.success) {
								_args.success(sessions);
							}

						} else {
							if(_args.onerror) {
								_args.onerror();
							} else {
								// noe error callback so use success callback to issue error
								var noData = {};
								noData.title = "Sorry, Nothing scheduled for this time / date";
								noData.description = "";
								noData.track = "";
								noData.sessioncode = "";
								noData.sessionnumber = "";
								noData.track = "";
								noData.end = "2011-10-23 00:00:00";
								noData.start = "2011-10-23 00:00:00";

								sessions.push(noData);

								// if callback was provided run it
								if(_args.success) {
									_args.success(sessions);
								}
							}
						}

					} catch(E) {
						Titanium.API.log(E);
					}
				};

				xhr.send();
			} else {
				Codestrong.ui.activityIndicator.hideModal();
				alert("no network connection detected");
			}
		},
		/*
		 * getSessionDetail
		 * params: _args (OBJECT)
		 * sub-params: uid (STRING), success (CALLBACK)
		 * return session (OBJECT)
		 */
		getSessionDetail : function(/*OBJECT*/_args) {

			if(Ti.Network.online) {
				var xhr = Titanium.Network.createHTTPClient();

				var url = "http://api.uwm.edu/index.cfm/hew/details/" + _args.uid + "?format=json";
				var json;
				xhr.open('GET', url);

				xhr.onerror = function(e) {
					Ti.API.error(e);

				};

				xhr.onload = function() {
					try {
						json = JSON.parse(this.responseText);
						var session = [];

						/*
						 * TODO: add check to makesure we recieved a vaild session
						 */

						if(json) {
							// clean up the output and return it
							session.title = json.TITLE;
							session.presenter = [];
							if(json.PRESENTERS.length > 1) {
								for(var i = 0; i < json.PRESENTERS.length; i++) {
									session.presenter[i] = {
										name : json.PRESENTERS[i].NAME,
										org : json.PRESENTERS[i].ORGANIZATION,
										jobtitle : json.PRESENTERS[i].JOBTITLE,
										bio : json.PRESENTERS[i].BIOGRAPHY
									};
								}
							} else if(json.PRESENTERS.length === 1) {
								session.presenter[0] = {
									name : json.PRESENTERS[0].NAME,
									org : json.PRESENTERS[0].ORGANIZATION,
									jobtitle : json.PRESENTERS[0].JOBTITLE,
									bio : json.PRESENTERS[0].BIOGRAPHY
								};
							}
							session.description = json.DESCRIPTION;
							if(json.SESSIONCODE) {
								session.sessioncode = json.SESSIONCODE;
							}
							session.day = json.TIME.DAY;
							session.start = json.TIME.START;
							session.end = json.TIME.END;
							session.room = json.LOCATION;
							session.uid = _args.uid;

							// if a callback was provided run it
							if(_args.success) {
								_args.success(session);
							}
						} else {
							if(_args.onerror) {
								_args.onerror();
							} else {
								// noe error callback so use success callback to issue error
								
								session.title = "Could not retrieve session";
								session.presenter = [];

								// if callback was provided run it
								if(_args.success) {
									_args.success(session);
								}
							}
						}
					} catch(E) {
						Titanium.API.log(E);
					}
				};

				xhr.send();
			} else {
				Codestrong.ui.activityIndicator.hideModal();
				alert("no network connection detected");
			}
		}
	};

})();
