/**
 * This file is part of HEWEB11 Mobile.
 *
 * HEWEB11 Mobile is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HEWEB11 Mobile is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HEWEB11 Mobile.  If not, see <http://www.gnu.org/licenses/>.
 *
 * The HEWEB11 mobile app was based off the original work done by Appcelerator Developer Relations and the team
 * at palatir.net
 *
 * CODESTRONG code can be located at: https://github.com/appcelerator/Codestrong
 * DRUPALCON code can be located at: https://github.com/palantirnet/drupalcon_mobile
 * 
 */
(function () {

    Codestrong.ui.createDayWindow = function (tabGroup) {
        // Base row properties
        var baseRow = {
            hasChild: true,
            color: '#000',
            backgroundColor: '#fff',
            font: {
                fontWeight: 'bold'
            }
        };
        baseRow[Codestrong.ui.backgroundSelectedProperty + 'Color'] = Codestrong.ui.backgroundSelectedColor;

        // Creates a TableViewRow using the base row properties and a given
        // params object
        var createDayRow = function (params) {
            return Codestrong.extend(Ti.UI.createTableViewRow(params), baseRow);
        };

        // Create data for TableView
        var data = [
	        createDayRow({
	            title: 'Right Now / Up Next',
	            titleShort: 'Right Now / Up Next',
	            api_date:'recent',
	            scheduleListing: true
	        }), createDayRow({
	            title: 'Sunday, October 23rd',
	            titleShort: 'October 23rd',
	            api_date:'date=20111023',
	            scheduleListing: true
	        }), createDayRow({
	            title: 'Monday, October 24th',
	            titleShort: 'October 24th',
	            api_date:'date=20111024',
	            scheduleListing: true
	        }), createDayRow({
	            title: 'Tuesday, October 25th',
	            titleShort: 'October 25th',
	            api_date:'date=20111025',
	            scheduleListing: true
	        }), createDayRow({
	            title: 'Wednesday, October 26th',
	            titleShort: 'October 26th',
	            start_date: '2011-10-26 00:00:00',
	            end_date: '2011-10-27 00:00:00',
	            api_date:'date=20111026',
	            scheduleListing: true
	        })
        ];

        // create main day window
        var dayWindow = Titanium.UI.createWindow({
            id: 'win1',
            title: 'Schedule',
            backgroundColor: '#fff',
            barColor: '#414444',
            fullscreen: false
        });
        var tableview = Titanium.UI.createTableView({
            data: data
        });
        dayWindow.add(tableview);

        tableview.addEventListener('click', function (e) {
            if (e.rowData.scheduleListing) {
                Codestrong.navGroup.open(Codestrong.ui.createSessionsWindow({
                    titleShort: e.rowData.titleShort,
                    title: e.rowData.title,
                    api_date: e.rowData.api_date
                }), {
                    animated: true
                });
            } else {
                Codestrong.navGroup.open(Codestrong.ui.createHtmlWindow({
                    title: e.rowData.titleShort,
                    url: e.rowData.url
                }), {
                    animated: true
                });
            }

        });

        return dayWindow;
    };
})();