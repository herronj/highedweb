/**
 * This file is part of HEWEB11 Mobile.
 *
 * HEWEB11 Mobile is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HEWEB11 Mobile is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HEWEB11 Mobile.  If not, see <http://www.gnu.org/licenses/>.
 *
 * The HEWEB11 mobile app was based off the original work done by Appcelerator Developer Relations and the team
 * at palatir.net
 *
 * CODESTRONG code can be located at: https://github.com/appcelerator/Codestrong
 * DRUPALCON code can be located at: https://github.com/palantirnet/drupalcon_mobile
 * 
 */
(function () {
    Codestrong.ui.createPresenterDetailWindow = function (settings) {
        Drupal.setDefaults(settings, {
            title: 'title here',
            name: '',
            compnay: '',
            jobtitle: '',
            bio: ''
        });

        var presenterDetailWindow = Titanium.UI.createWindow({
            id: 'presenterDetailWindow',
            title: settings.name,
            backgroundColor: '#FFF',
            barColor: '#414444',
            fullscreen: false
        });
        presenterDetailWindow.orientationModes = [Ti.UI.PORTRAIT];

        var tvData = [];
        var tv = Ti.UI.createTableView({
            textAlign: 'left',
            width: '100%',
            separatorColor: '#fff'
        });
        tv.footerView = Ti.UI.createView({
            height: 1,
            opacity: 0
        });

        var av = Ti.UI.createImageView({
            image: 'images/userpict-large@2x.png',
            left: 0,
            top: 0,
            height: 110,
            width: 110,
            defaultImage: 'images/userpict-large@2x.png',
            backgroundColor: '#000',
            touchEnabled: false
        });
        var headerRow = Ti.UI.createTableViewRow({
            height: 110,
            backgroundImage: 'images/sessionbckgd@2x.png',
            className: 'presHeaderRow',
            left: 0,
            top: -5,
            bottom: 0,
            layout: 'vertical',
            selectionStyle: 'none'
        });
        headerRow[Codestrong.ui.backgroundSelectedProperty + 'Image'] = 'images/sessionbckgd@2x.png';

        var bioRow = Ti.UI.createTableViewRow({
            hasChild: false,
            height: 'auto',
            width: '100%',
            selectionStyle: 'none'
        });

        // Add the avatar image to the view
        headerRow.add(av);

        if (settings.name !== undefined) {
            var fullName = Ti.UI.createLabel({
                text: Codestrong.cleanSpecialChars(settings.name),
                font: {
                    fontSize: 20,
                    fontWeight: 'bold'
                },
                textAlign: 'left',
                color: '#000',
                height: 'auto',
                left: 120,
                top: -95,
                ellipsize: true,
                touchEnabled: false
            });
            headerRow.add(fullName);
        }

        if (settings.company !== undefined) {
            var company = Ti.UI.createLabel({
                text: Codestrong.cleanSpecialChars(settings.company),
                font: {
                    fontSize: 14,
                    fontWeight: 'bold'
                },
                textAlign: 'left',
                color: '#666',
                height: 'auto',
                left: 120,
                touchEnabled: false
            });
            headerRow.add(company);
        }
        tvData.push(headerRow);

        var bioText = (!settings.bio) ? "No biography available" : Codestrong.cleanSpecialChars(settings.bio.replace(/^[\s\n\r\t]+|[\s\n\r\t]+$/g, '').replace(/\n/g, "\n\n"));
        var bio = Ti.UI.createLabel({
            text: bioText,
            backgroundColor: '#fff',
            textAlign: 'left',
            color: '#000',
            height: 'auto',
            width: Codestrong.isAndroid() ? '92%' : 'auto',
            top: 10,
            bottom: 10,
            font: {
                fontSize: 16
            }
        });

        if (!Codestrong.isAndroid()) {
            bio.right = 10;
            bio.left = 10;
        }

        bioRow.add(bio);
        tvData.push(Codestrong.ui.createHeaderRow('Biography'));
        tvData.push(bioRow);

        tv.setData(tvData);
        presenterDetailWindow.add(tv);

        return presenterDetailWindow;
    };

})();