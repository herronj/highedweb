/**
 * This file is part of HEWEB11 Mobile.
 *
 * HEWEB11 Mobile is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HEWEB11 Mobile is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HEWEB11 Mobile.  If not, see <http://www.gnu.org/licenses/>.
 *
 * The HEWEB11 mobile app was based off the original work done by Appcelerator Developer Relations and the team
 * at palatir.net
 *
 * CODESTRONG code can be located at: https://github.com/appcelerator/Codestrong
 * DRUPALCON code can be located at: https://github.com/palantirnet/drupalcon_mobile
 *
 */
(function() {

	Codestrong.ui.createSessionsWindow = function(settings) {
		var scheduleTimeout = 11000;

		Drupal.setDefaults(settings, {
			title : 'title here',
			api_date : ''
		});

		var sessionsWindow = Titanium.UI.createWindow({
			id : 'sessionsWindow',
			title : settings.titleShort,
			backgroundColor : '#FFF',
			barColor : '#414444',
			fullscreen : false
		});
		sessionsWindow.orientationModes = [Ti.UI.PORTRAIT];
		
		// create table view
		var tableview = Titanium.UI.createTableView({
			backgroundColor : '#fff',
			layout : 'vertical'
		});
		
		var data = [];
		// Create session rows
		var lastTime = '';
		// var sessions = Drupal.entity.db('main', 'node').loadMultiple(nids, ['start_date', 'nid']);
		Codestrong.ui.activityIndicator.showModal('Loading Schedule data...', scheduleTimeout, 'Schedule API timed out. please try again later.');
		hew.schedule.getSessions({
			date : settings.api_date,
			success : function(sessions) {
				for(var sessionNum = 0, numSessions = sessions.length; sessionNum < numSessions; sessionNum++) {
					var session = sessions[sessionNum];
					var sessionTitle = Codestrong.cleanSpecialChars(session.title);
					var sessionRow = Ti.UI.createTableViewRow({
						hasChild : (session.uid) ? true : false,
						className : 'hew_session',
						selectedColor : '#000',
						backgroundColor : '#fff',
						color : '#000',
						start_date : session.start,
						end_date : session.end,
						uid : session.uid,
						sessionTitle : sessionTitle,
						height : 'auto',
						layout : 'vertical',
						focusable : true
					});
					sessionRow[Codestrong.ui.backgroundSelectedProperty + 'Color'] = Codestrong.ui.backgroundSelectedColor;

					var leftSpace = 10;
					var titleColor = '';
					switch (session.track) {
						case "TNT":
							titleColor = '#DB9130';
							break;
						case "APS":
							titleColor = '#75A7D9';
							break;
						case "SOC":
							titleColor = '#CC5A5A';
							break;
						case "MMP":
							titleColor = '#8CC63F';
							break;
						case "TPR":
							titleColor = '#905FD9';
							break;
						default:
							titleColor = '#353535';
							break;
					}

					// If there is a new session time, insert a header in the table.
					var headerRow = undefined;
					if(lastTime === '' || session.start !== lastTime) {
						lastTime = session.start;
						headerRow = Codestrong.ui.createHeaderRow(Codestrong.datetime.cleanTime(lastTime) + " - " + Codestrong.datetime.cleanTime(session.end));
					}

					var titleLabel = Ti.UI.createLabel({
						text : sessionTitle,
						font : {
							fontSize : 16,
							fontWeight : 'bold'
						},
						color : titleColor,
						left : leftSpace,
						top : 10,
						right : 10,
						height : 'auto',
						touchEnabled : false
					});
					sessionRow.add(titleLabel);

					if(session.sessioncode != "NONE") {
						var trackLabel = Ti.UI.createLabel({
							text : session.sessioncode,
							font : {
								fontSize : 14,
								fontWeight : 'normal'
							},
							color : '#000',
							left : leftSpace,
							top : 4,
							bottom : 0,
							right : 10,
							height : 'auto'
						});
						sessionRow.add(trackLabel);
					}

					if(headerRow) {
						data.push(headerRow);
					}
					data.push(sessionRow);
				}
				tableview.setData(data);
				// hide activity Ind after a second
				setTimeout(function() {
					Codestrong.ui.activityIndicator.hideModal();
				}, 1000);
			}
		});

		// Create table view event listener.
		tableview.addEventListener('click', function(e) {
			if(e.rowData.uid) {
				Codestrong.navGroup.open(Codestrong.ui.createSessionDetailWindow({
					title : e.rowData.sessionTitle,
					uid : e.rowData.uid
				}), {
					animated : true
				});
			}
		});
		// add table view to the window
		sessionsWindow.add(tableview);

		return sessionsWindow;
	};
})();
