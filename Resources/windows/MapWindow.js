/**
 * This file is part of HEWEB11 Mobile.
 *
 * HEWEB11 Mobile is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HEWEB11 Mobile is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HEWEB11 Mobile.  If not, see <http://www.gnu.org/licenses/>.
 *
 * The HEWEB11 mobile app was based off the original work done by Appcelerator Developer Relations and the team
 * at palatir.net
 *
 * CODESTRONG code can be located at: https://github.com/appcelerator/Codestrong
 * DRUPALCON code can be located at: https://github.com/palantirnet/drupalcon_mobile
 *
 */
(function() {

	Codestrong.ui.createMapWindow = function() {
		var mapWindow = Titanium.UI.createWindow({
			id : 'mapWindow',
			title : 'Meeting Room Maps',
			backgroundColor : '#FFF',
			barColor : '#414444',
			height : '100%',
			fullscreen : false
		});

		// create table view data object
		var duration = 250;
		var data = [{
			title : 'Floor 3 - Grand Ballroom',
			shortTitle : '1st Floor',
			url : 'pages/maps/map1.html',
			animateOut : {
				left : -1 * Ti.Platform.displayCaps.platformWidth,
				top : Codestrong.ui.tabBarHeight,
				duration : duration
			},
			animateIn : {
				left : 0,
				top : Codestrong.ui.tabBarHeight,
				duration : duration
			},
			left : 0
		}, {
			title : 'Floor 2',
			shortTitle : '2nd Floor',
			url : 'pages/maps/map2.html',
			animateOut : {
				left : -1 * Ti.Platform.displayCaps.platformWidth,
				top : Codestrong.ui.tabBarHeight,
				duration : duration
			},
			animateIn : {
				left : 0,
				top : Codestrong.ui.tabBarHeight,
				duration : duration
			},
			left : Ti.Platform.displayCaps.platformWidth
		}, {
			title : 'Floor 17',
			shortTitle : '17th Floor',
			url : 'pages/maps/map17.html',
			animateOut : {
				left : Ti.Platform.displayCaps.platformWidth,
				top : Codestrong.ui.tabBarHeight,
				duration : duration
			},
			animateIn : {
				left : 0,
				top : Codestrong.ui.tabBarHeight,
				duration : duration
			},
			left : Ti.Platform.displayCaps.platformWidth
		}];
		var dataLength = 4;
		//override

		var tabbedBarView = Ti.UI.createView({
			backgroundColor : '#555',
			top : 0,
			height : Codestrong.ui.tabBarHeight
		});
		var tabbedBar = Ti.UI.createView({
			top : 0,
			backgroundColor : '#000',
			height : Codestrong.ui.tabBarHeight,
			width : Ti.Platform.displayCaps.platformWidth
		});

		for(var i = 0; i < data.length; i++) {
			var myEntry = data[i];

			myEntry.webview = Ti.UI.createWebView({
				scalesPageToFit : true,
				html: 'test',
				url : myEntry.url,
				top : Codestrong.ui.tabBarHeight,
				bottom : 0,
				backgroundColor: '#00ff00',
				left : myEntry.left,
				width : Ti.Platform.displayCaps.platformWidth
			});

			var tabView = Ti.UI.createView({
				backgroundImage : (i === 0) ? 'images/buttonbar/button2_selected.png' : 'images/buttonbar/button2_unselected_shadowL.png',
				height : Codestrong.ui.tabBarHeight,
				left : i * (Ti.Platform.displayCaps.platformWidth / dataLength),
				right : Ti.Platform.displayCaps.platformWidth - ((parseInt(i) + 1) * (Ti.Platform.displayCaps.platformWidth / dataLength)),
				index : i
			});

			var tabLabel = Ti.UI.createLabel({
				text : myEntry.shortTitle,
				textAlign : 'center',
				color : '#fff',
				height : 'auto',
				touchEnabled : false,
				font : {
					fontSize : 14,
					fontWeight : 'bold'
				}
			});
			tabView.addEventListener('click', function(e) {
				
				data[e.source.index].webview.fireEvent("doubletap");
				data[e.source.index].webview.fireEvent("dblclick");
				
				
				if(e.source.index === 0) {
					data[0].tabView.backgroundImage = 'images/buttonbar/button2_selected.png';
					data[1].tabView.backgroundImage = 'images/buttonbar/button2_unselected_shadowL.png';
					data[2].tabView.backgroundImage = 'images/buttonbar/button2_unselected_shadowL.png';
					data[3].tabView.backgroundImage = 'images/buttonbar/button2_unselected_shadowL.png';
				} else if(e.source.index === 1) {
					data[0].tabView.backgroundImage = 'images/buttonbar/button2_unselected_shadowR.png';
					data[1].tabView.backgroundImage = 'images/buttonbar/button2_selected.png';
					data[2].tabView.backgroundImage = 'images/buttonbar/button2_unselected_shadowL.png';
					data[3].tabView.backgroundImage = 'images/buttonbar/button2_unselected_shadowL.png';
				} else if(e.source.index === 2) {
					data[0].tabView.backgroundImage = 'images/buttonbar/button2_unselected_shadowR.png';
					data[1].tabView.backgroundImage = 'images/buttonbar/button2_unselected_shadowL.png';
					data[2].tabView.backgroundImage = 'images/buttonbar/button2_selected.png.png';
					data[3].tabView.backgroundImage = 'images/buttonbar/button2_unselected_shadowL.png';
				} else if(e.source.index === 3) {
					data[0].tabView.backgroundImage = 'images/buttonbar/button2_unselected_shadowR.png';
					data[1].tabView.backgroundImage = 'images/buttonbar/button2_unselected_shadowL.png';
					data[2].tabView.backgroundImage = 'images/buttonbar/button2_unselected_shadowL.png';
					data[3].tabView.backgroundImage = 'images/buttonbar/button2_selected.png.png';

				}

				for(var j = 0; j < dataLength; j++) {
					if(e.source.index === 0) {
						data[0].webview.animate(data[0].animateIn);
						data[1].webview.animate(data[1].animateOut);
						data[2].webview.animate(data[2].animateOut);
						data[3].webview.animate(data[3].animateOut);
					} else if(e.source.index === 1) {
						data[0].webview.animate(data[0].animateOut);
						data[1].webview.animate(data[1].animateIn);
						data[2].webview.animate(data[2].animateOut);
						data[3].webview.animate(data[3].animateOut);
					} else if(e.source.index === 2) {
						data[0].webview.animate(data[0].animateOut);
						data[1].webview.animate(data[1].animateOut);
						data[2].webview.animate(data[2].animateIn);
						data[3].webview.animate(data[3].animateOut);
					} else if(e.source.index === 3) {
						data[0].webview.animate(data[0].animateOut);
						data[1].webview.animate(data[1].animateOut);
						data[2].webview.animate(data[2].animateOut);
						data[3].webview.animate(data[3].animateIn);
					}
				}
			});

			tabView.add(tabLabel);
			tabbedBar.add(tabView);
			myEntry.tabView = tabView;
		}

		//Local Map
		var mapView = Titanium.Map.createView({
			scalesPageToFit : true,
			top : Codestrong.ui.tabBarHeight,
			bottom : 0,
			left : Ti.Platform.displayCaps.platformWidth,
			width : Ti.Platform.displayCaps.platformWidth,
			mapType : Titanium.Map.STANDARD_TYPE,
			region : {
				latitude : 30.26074,
				longitude : -97.74712,
				latitudeDelta : 0.002302,
				longitudeDelta : 0.001362
			},
			animate : true,
			userLocation : true
		});

		var hotelMarker = Titanium.Map.createAnnotation({
			latitude : 30.26074,
			longitude : -97.74712,
			title : "Hyatt Regency Austin",
			subtitle : "Conference Venue & Hotel",
			animate : true
		});
		var altHotelMarker = Titanium.Map.createAnnotation({
			latitude : 30.26553,
			longitude : -97.74062,
			title : "Marriott Courtyard Austin Downtown",
			subtitle : "Alternate Hotel",
			animate : true
		});
		var airportMarker = Titanium.Map.createAnnotation({
			latitude : 30.20272,
			longitude : -97.66751,
			title : "Austin-Bergstrom International Airport",
			animate : true
		});
		var billardsMarker = Titanium.Map.createAnnotation({
			latitude : 30.26715,
			longitude : -97.74306,
			title : "Buffalo Billards",
			subtitle : "Monday Night: HighEdWeb After Dark: LINK Party",
			animate : true
		});
		var excurMarker = Titanium.Map.createAnnotation({
			latitude : 30.25524,
			longitude : -97.76200,
			title : "The Highball",
			subtitle : "Tuesday Night Excursion: The HighBall",
			animate : true
		});

		mapView.addAnnotation(hotelMarker);
		mapView.addAnnotation(altHotelMarker);
		mapView.addAnnotation(airportMarker);
		mapView.addAnnotation(billardsMarker);
		mapView.addAnnotation(excurMarker);

		data[3] = {
			title : 'Austin',
			shortTitle : 'Austin',
			webview : mapView,
			animateOut : {
				left : Ti.Platform.displayCaps.platformWidth,
				top : Codestrong.ui.tabBarHeight,
				duration : duration
			},
			animateIn : {
				left : 0,
				top : Codestrong.ui.tabBarHeight,
				duration : duration
			},
			left : Ti.Platform.displayCaps.platformWidth
		};

		var tabView2 = Ti.UI.createView({
			backgroundImage : 'images/buttonbar/button2_unselected_shadowL.png',
			height : 36,
			left : 3 * (Ti.Platform.displayCaps.platformWidth / dataLength),
			right : Ti.Platform.displayCaps.platformWidth - (4 * (Ti.Platform.displayCaps.platformWidth / dataLength)),
			index : 3
		});

		var tabLabel2 = Ti.UI.createLabel({
			text : "Austin",
			textAlign : 'center',
			color : '#fff',
			height : 'auto',
			touchEnabled : false,
			font : {
				fontSize : 14,
				fontWeight : 'bold'
			}
		});

		tabView2.addEventListener('click', function(e) {
			if(e.source.index === 0) {
				data[0].tabView.backgroundImage = 'images/buttonbar/button2_selected.png';
				data[1].tabView.backgroundImage = 'images/buttonbar/button2_unselected_shadowL.png';
				data[2].tabView.backgroundImage = 'images/buttonbar/button2_unselected_shadowL.png';
				data[3].tabView.backgroundImage = 'images/buttonbar/button2_unselected_shadowL.png';
			} else if(e.source.index === 1) {
				data[0].tabView.backgroundImage = 'images/buttonbar/button2_unselected_shadowR.png';
				data[1].tabView.backgroundImage = 'images/buttonbar/button2_selected.png';
				data[2].tabView.backgroundImage = 'images/buttonbar/button2_unselected_shadowL.png';
				data[3].tabView.backgroundImage = 'images/buttonbar/button2_unselected_shadowL.png';
			} else if(e.source.index === 2) {
				data[0].tabView.backgroundImage = 'images/buttonbar/button2_unselected_shadowR.png';
				data[1].tabView.backgroundImage = 'images/buttonbar/button2_unselected_shadowL.png';
				data[2].tabView.backgroundImage = 'images/buttonbar/button2_selected.png.png';
				data[3].tabView.backgroundImage = 'images/buttonbar/button2_unselected_shadowL.png';
			} else if(e.source.index === 3) {
				data[0].tabView.backgroundImage = 'images/buttonbar/button2_unselected_shadowR.png';
				data[1].tabView.backgroundImage = 'images/buttonbar/button2_unselected_shadowL.png';
				data[2].tabView.backgroundImage = 'images/buttonbar/button2_unselected_shadowL.png';
				data[3].tabView.backgroundImage = 'images/buttonbar/button2_selected.png.png';

			}

			for(var j = 0; j < dataLength; j++) {
				if(e.source.index === 0) {
					data[0].webview.animate(data[0].animateIn);
					data[1].webview.animate(data[1].animateOut);
					data[2].webview.animate(data[2].animateOut);
					data[3].webview.animate(data[3].animateOut);
				} else if(e.source.index === 1) {
					data[0].webview.animate(data[0].animateOut);
					data[1].webview.animate(data[1].animateIn);
					data[2].webview.animate(data[2].animateOut);
					data[3].webview.animate(data[3].animateOut);
				} else if(e.source.index === 2) {
					data[0].webview.animate(data[0].animateOut);
					data[1].webview.animate(data[1].animateOut);
					data[2].webview.animate(data[2].animateIn);
					data[3].webview.animate(data[3].animateOut);
				} else if(e.source.index === 3) {
					data[0].webview.animate(data[0].animateOut);
					data[1].webview.animate(data[1].animateOut);
					data[2].webview.animate(data[2].animateOut);
					data[3].webview.animate(data[3].animateIn);
				}
			}
		});

		tabView2.add(tabLabel2);
		tabbedBar.add(tabView2);
		data[3].tabView = tabView2;

		tabbedBarView.add(tabbedBar);
		mapWindow.add(tabbedBarView);
		mapWindow.add(data[0].webview);
		mapWindow.add(data[1].webview);
		mapWindow.add(data[2].webview);
		mapWindow.add(data[3].webview);
		
		data[0].webview.show();

		return mapWindow;
	};
})();
