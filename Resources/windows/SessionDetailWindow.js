/**
 * This file is part of HEWEB11 Mobile.
 *
 * HEWEB11 Mobile is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * HEWEB11 Mobile is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with HEWEB11 Mobile.  If not, see <http://www.gnu.org/licenses/>.
 *
 * The HEWEB11 mobile app was based off the original work done by Appcelerator Developer Relations and the team
 * at palatir.net
 *
 * CODESTRONG code can be located at: https://github.com/appcelerator/Codestrong
 * DRUPALCON code can be located at: https://github.com/palantirnet/drupalcon_mobile
 *
 */
(function() {
	var detailTimeout = 11000;
	Codestrong.ui.createSessionDetailWindow = function(settings) {
		Drupal.setDefaults(settings, {
			title : 'title here',
			uid : ''
		});
		var commonPadding = 15;
		var sessionDetailWindow = Titanium.UI.createWindow({
			id : 'sessionDetailWindow',
			title : settings.title,
			backgroundColor : '#FFF',
			barColor : '#414444',
			fullscreen : false
		});
		sessionDetailWindow.orientationModes = [Ti.UI.PORTRAIT];

		// Build session data
		// var sessionData = Drupal.entity.db('main', 'node').load(settings.nid);

		var tvData = [];
		var tv = Ti.UI.createTableView({
			textAlign : 'left',
			layout : 'vertical',
			separatorColor : '#fff'
		});
		tv.footerView = Ti.UI.createView({
			height : 1,
			opacity : 0
		});
		
		Codestrong.ui.activityIndicator.showModal('Loading Session data...', detailTimeout, 'Schedule API timed out. please try again later.');
		hew.schedule.getSessionDetail({
			uid : settings.uid,
			success : function(session) {
				renderSession(session);
			}
		});

		var renderSession = function(sessionData) {

			var headerRow = Ti.UI.createTableViewRow({
				height : 'auto',
				left : 0,
				top : -5,
				bottom : 10,
				layout : 'vertical',
				className : 'mainHeaderRow',
				backgroundImage : 'images/sessionbckgd@2x.png',
				backgroundPosition : 'bottom left',
				selectionStyle : 'none'
			});
			headerRow[Codestrong.ui.backgroundSelectedProperty + 'Image'] = 'images/sessionbckgd@2x.png';

			var bodyRow = Ti.UI.createTableViewRow({
				hasChild : false,
				height : 'auto',
				backgroundColor : '#fff',
				left : 0,
				top : -5,
				bottom : 10,
				layout : 'vertical',
				className : 'bodyRow',
				selectionStyle : 'none'
			});

			if(sessionData.title) {
				var titleLabel = Ti.UI.createLabel({
					text : Codestrong.cleanSpecialChars(sessionData.title),
					font : {
						fontSize : 28,
						fontWeight : 'bold'
					},
					textAlign : 'left',
					color : '#000',
					left : commonPadding,
					top : 18,
					bottom : 7,
					right : commonPadding,
					height : 'auto'
				});
				headerRow.add(titleLabel);
			}

			if(sessionData.day) {
				var formatDate;
				if(sessionData.end) {
					formatDate = sessionData.day + ", " + sessionData.start + " - " + sessionData.end;
				} else {
					formatDate = sessionData.day + ", " + sessionData.start;
				}
				var datetime = Ti.UI.createLabel({
					text : formatDate,
					font : {
						fontSize : 18,
						fontWeight : 'normal'
					},
					textAlign : 'left',
					color : '#000',
					left : commonPadding,
					top : 'auto',
					bottom : 5,
					right : 'auto',
					height : 'auto'
				});
				headerRow.add(datetime);
			}

			if(sessionData.sessioncode) {
				var track = Ti.UI.createLabel({
					text : sessionData.sessioncode,
					font : {
						fontSize : 18,
						fontWeight : 'normal'
					},
					textAlign : 'left',
					color : '#000',
					left : commonPadding,
					top : 'auto',
					bottom : 5,
					right : 'auto',
					height : 'auto'
				});
				headerRow.add(track);
			}

			var skipRoom;
			if(sessionData.title === 'Breakfest' || sessionData.title === 'Lunch' || sessionData.title === 'Refreshment Break' || sessionData.title === 'HighEdWeb After Dark') {
				skipRoom = true;
			}

			if(sessionData.room && !skipRoom) {
				var room = Ti.UI.createLabel({
					text : Codestrong.cleanSpecialChars(sessionData.room),
					font : {
						fontSize : 18,
						fontWeight : 'normal'
					},
					textAlign : 'left',
					color : '#000',
					left : commonPadding,
					top : 'auto',
					bottom : 12,
					right : commonPadding,
					height : 'auto'
				});
				headerRow.add(room);
			}

			if(sessionData.description) {
				var body = Ti.UI.createLabel({
					text : Codestrong.cleanSpecialChars(sessionData.description.replace('\n', '\n\n')),
					backgroundColor : '#fff',
					textAlign : 'left',
					color : '#000',
					height : 'auto',
					width : Codestrong.isAndroid() ? '92%' : 'auto',
					top : 15,
					bottom : 15,
					font : {
						fontSize : 16
					}
				});
				
				if(!Codestrong.isAndroid()) {
					body.right = commonPadding;
					body.left = commonPadding;
				}
				
				bodyRow.add(body);
			}

			

			tvData.push(headerRow);

			if(sessionData.presenter[0]) {
				var presenterList = sessionData.presenter;

				tvData.push(Codestrong.ui.createHeaderRow((presenterList.length > 1) ? 'Speakers' : 'Speaker'));
				for(var j in presenterList) {
					if (presenterList[j]) {
						tvData.push(renderPresenter(presenterList[j]));
					}
				}
			}

			if(sessionData.sessioncode && sessionData.sessioncode !== 'NONE') {
				var feedbackTitle = Ti.UI.createLabel({
					text : "Give session Feedback",
					backgroundColor : '#3782a8',
					textAlign : 'left',
					font : {
						fontSize : 18,
						fontWeight : 'bold'
					},
					color : '#fff',
					left : commonPadding,
					right : commonPadding,
					height : 50
				});

				var feedbackRow = Ti.UI.createTableViewRow({
					hasChild : true,
					layout : 'vertical',
					height : 50,
					className : 'feedbackRow',
					backgroundColor : '#3782A9'
				});
				feedbackRow.add(feedbackTitle);

				feedbackRow.addEventListener('click', function(e) {
					
					var track = sessionData.sessioncode.replace(/[0-9]/g, "");
					var sessionNumber = sessionData.sessioncode.replace(/[A-Za-z]/g, "");
					
					Titanium.API.debug(track+"//"+sessionNumber);
					
					Codestrong.navGroup.open(Codestrong.ui.createHtmlWindow({
						title : settings.title,
						//url : 'https://docs.google.com/spreadsheet/embeddedform?formkey=dFFGbVBpZ1BaTFJXRFhmdS1BY2F1cnc6MQ&entry_0=' + sessionData.sessioncode
						url : 'http://api.uwm.edu/index.cfm/hew/feedback?entry_0=' + track + '&entry_15=' + sessionNumber + '&entry_17=' + Titanium.Platform.macaddress
					}), {
						animated : true
					});
				});

				tvData.push(feedbackRow);
			}

			tvData.push(Codestrong.ui.createHeaderRow('Description'));
			tvData.push(bodyRow);

			tv.addEventListener('click', function(e) {
				if(e.source.presenter !== undefined) {
					var fullName = e.source.presenter.name || '';
					Codestrong.navGroup.open(Codestrong.ui.createPresenterDetailWindow({
						title : fullName,
						name : fullName,
						company : e.source.presenter.org,
						bio : e.source.presenter.bio,
						jobtitle : e.source.presenter.jobtitle
					}), {
						animated : true
					});
				}
			});
			tv.setData(tvData);
			
			setTimeout(function() {
				Codestrong.ui.activityIndicator.hideModal();
			}, 1000);
		}


		sessionDetailWindow.add(tv);

		return sessionDetailWindow;
	};
	function renderPresenter(presenter) {
		var userPict = 'images/userpict-large.png';

		var av = Ti.UI.createImageView({
			image : userPict,
			left : 5,
			top : 5,
			height : 50,
			width : 50,
			defaultImage : 'images/userpict-large.png',
			backgroundColor : '#000',
			touchEnabled : false
		});

		var presRow = Ti.UI.createTableViewRow({
			presenter : presenter,
			height : 60,
			className : 'presenterRow',
			borderColor : '#C4E2EF',
			hasChild : true,
			backgroundColor : '#CE3016',
			layout : 'vertical'
		});
		presRow[Codestrong.ui.backgroundSelectedProperty + 'Color'] = Codestrong.ui.backgroundSelectedColor;

		presRow.add(av);
		var presenterFullName2 = Ti.UI.createLabel({
			presenter : presenter,
			text : Codestrong.cleanSpecialChars(presenter.name),
			font : {
				fontSize : 18,
				fontWeight : 'bold'
			},
			left : 75,
			top : -45,
			height : 'auto',
			color : '#fff',
			touchEnabled : false
		});

		var presenterName2 = Ti.UI.createLabel({
			presenter : presenter,
			text : Codestrong.cleanSpecialChars(presenter.org),
			font : {
				fontSize : 12,
				fontWeight : 'normal'
			},
			left : 75,
			bottom : 10,
			height : 'auto',
			color : "#fff",
			touchEnabled : false
		});

		presRow.add(presenterFullName2);
		presRow.add(presenterName2);

		return presRow;
	}

})();
